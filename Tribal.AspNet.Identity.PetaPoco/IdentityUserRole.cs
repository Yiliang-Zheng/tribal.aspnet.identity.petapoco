﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribal.AspNet.Identity.PetaPoco.Attributes;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco
{
    //
    // Summary:
    //     EntityType that represents a user belonging to a role
    //
    // Type parameters:
    //   TKey:
    [TableName("AspNetUserRoles")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class IdentityUserRole<TKey>
    {
        public virtual int Id { get; set; }
        public IdentityUserRole() { }

        //
        // Summary:
        //     RoleId for the role
        [ForeignKey(OnCascadeDelete = true, Reference = typeof(IdentityRole<,>))]
        public virtual TKey RoleId { get; set; }
        //
        // Summary:
        //     UserId for the user that is in the role
        [ForeignKey(OnCascadeDelete = true, Reference = typeof(IdentityUser<,,,>))]
        public virtual TKey UserId { get; set; }
    }

    public class IdentityUserRole : IdentityUserRole<Guid>
    {
        public IdentityUserRole() : base()
        {
            
        }
    }
}
