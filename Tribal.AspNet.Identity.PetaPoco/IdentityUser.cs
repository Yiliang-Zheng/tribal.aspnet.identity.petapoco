﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco
{
    /// <summary>
    ///     Default IUser implementation
    /// </summary>
    /// <typeparam name="TKey">TKey</typeparam>
    /// <typeparam name="TLogin">TLogin</typeparam>
    /// <typeparam name="TRole">TRole</typeparam>
    /// <typeparam name="TClaim">TClaim</typeparam>
    [TableName("AspNetUsers")]
    [PrimaryKey("Id", AutoIncrement = false)]
    public class IdentityUser<TKey, TLogin, TRole, TClaim> : IUser<TKey>
        where TLogin : IdentityUserLogin<TKey>
        where TRole : IdentityUserRole<TKey>
        where TClaim : IdentityUserClaim<TKey>
    {
        /// <summary>
        ///     Constructor 
        /// </summary>
        public IdentityUser()
        {
        }
        /// <summary>
        /// Constructor with username param
        /// </summary>
        /// <param name="userName"></param>
        public IdentityUser(string userName)
            : this()
        {
            UserName = userName;
        }
        /// <summary>
        ///     Used to record failures for the purposes of lockout
        /// </summary>
        public virtual int AccessFailedCount { get; set; }
        //
        // Summary:
        //     Navigation property for user claims
        [Ignore]
        public virtual ICollection<TClaim> Claims { get; }
        /// <summary>
        ///     Email
        /// </summary>
        public virtual string Email { get; set; }
        /// <summary>
        ///     True if the email is confirmed, default is false
        /// </summary>
        public virtual bool EmailConfirmed { get; set; }
        /// <summary>
        /// User Id (Primary Key)
        /// </summary>
        public virtual TKey Id { get; set; }
        /// <summary>
        ///     Is two factor enabled for the user
        /// </summary>
        public virtual bool TwoFactorEnabled { get; set; }
        /// <summary>
        ///     DateTime in UTC when lockout ends, any time in the past is considered not locked out.
        /// </summary>
        public virtual DateTime? LockoutEndDateUtc { get; set; }
        //
        // Summary:
        //     Navigation property for user logins
        [Ignore]
        public virtual ICollection<TLogin> Logins { get; }
        /// <summary>
        ///     The salted/hashed form of the user password
        /// </summary>
        public virtual string PasswordHash { get; set; }
        /// <summary>
        ///     PhoneNumber for the user
        /// </summary>
        public virtual string PhoneNumber { get; set; }
        /// <summary>
        ///     True if the phone number is confirmed, default is false
        /// </summary>
        public virtual bool PhoneNumberConfirmed { get; set; }
        //
        // Summary:
        //     Navigation property for user roles
        [Ignore]
        public virtual ICollection<TRole> Roles { get; }
        /// <summary>
        ///     A random value that should change whenever a users credentials have changed (password changed, login removed)
        /// </summary>
        public virtual string SecurityStamp { get; set; }
        /// <summary>
        ///     Is lockout enabled for this user
        /// </summary>
        public virtual bool LockoutEnabled { get; set; }
        /// <summary>
        /// Username
        /// </summary>
        public string UserName { get; set; }
    }

    public class IdentityUser : IdentityUser<Guid, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
    {
        public IdentityUser() : base()
        {            
        }

        public IdentityUser(string username) : base(username)
        {            
        }
    }
}
