﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Tribal.Data.Entity;


namespace Tribal.AspNet.Identity.PetaPoco
{
    using Attributes;

    public class IdentityDbContext<TUser, TRole, TKey, TUserLogin, TUserRole, TUserClaim> : Database
        where TUser : IdentityUser<TKey, TUserLogin, TUserRole, TUserClaim>
        where TRole : IdentityRole<TKey, TUserRole>
        where TUserLogin : IdentityUserLogin<TKey>
        where TUserRole : IdentityUserRole<TKey>
        where TUserClaim : IdentityUserClaim<TKey>
    {
        public IdentityDbContext() : base()
        {
        }

        public IdentityDbContext(string connectionString) : base(connectionString)
        {
            OnModelCreating();
        }

        private void OnModelCreating()
        {
            using (var transaction = GetTransaction())
            {
                CreateTable<TUser>();
                CreateTable<TUserLogin>();
                CreateTable<TUserClaim>();
                CreateTable<TRole>();
                CreateTable<TUserRole>();
                CreateRelationships<TUser>();
                CreateRelationships<TRole>();
                transaction.Complete();
            }
        }

        public void CreateTable<T>() where T : class
        {
            var script = MapTablename(typeof(T));
            Execute(script);
        }

        private void CreateRelationships<T>() where T : class
        {
            var script = ConstructRelationship(typeof(T));
            Execute(script);
        }


        public string MapTablename(Type destinateTable)
        {
            var sb = new StringBuilder();

            //generate "create table" script
            if (destinateTable == typeof(TUser))
            {
                sb.AppendLine(
                    $"IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{typeof(TUser).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetUsers"}')");
                sb.AppendLine("BEGIN");
                sb.AppendLine($"CREATE TABLE [dbo].[{typeof(TUser).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetUsers"}] (");
            }

            if (destinateTable == typeof(TRole))
            {
                sb.AppendLine(
                    $"IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{typeof(TRole).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetRoles"}')");
                sb.AppendLine("BEGIN");
                sb.AppendLine($"CREATE TABLE [dbo].[{typeof(TRole).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetRoles"}] (");
            }

            if (destinateTable == typeof(TUserClaim))
            {
                sb.AppendLine(
                    $"IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{typeof(TUserClaim).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetUserClaims"}')");
                sb.AppendLine("BEGIN");
                sb.AppendLine($"CREATE TABLE [dbo].[{typeof(TUserClaim).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetUserClaims"}] (");
            }

            if (destinateTable == typeof(TUserLogin))
            {
                sb.AppendLine(
                    $"IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{typeof(TUserLogin).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetUserLogins"}')");
                sb.AppendLine("BEGIN");
                sb.AppendLine($"CREATE TABLE [dbo].[{typeof(TUserLogin).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetUserLogins"}] (");
            }

            if (destinateTable == typeof(TUserRole))
            {
                sb.AppendLine(
                    $"IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{typeof(TUserRole).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetuserRoles"}')");
                sb.AppendLine("BEGIN");
                sb.Append($"CREATE TABLE [dbo].[{typeof(TUserRole).GetCustomAttribute<TableNameAttribute>()?.Value ?? "AspNetuserRoles"}] (");
            }

            var properties = destinateTable.GetProperties();
            //generate column script
            foreach (var property in properties)
            {
                sb.AppendLine(GenerateColumnScript(property));
            }

            //genreate foreign key script
            //foreach (var property in properties.Where(p => p.GetCustomAttribute<ForeignKeyAttribute>() != null))
            //{
            //    var fkScript = GenerateForeignKeyScript(property, destinateTable);
            //    if (!string.IsNullOrEmpty(fkScript)) sb.AppendLine(fkScript);
            //}

            sb.AppendLine(GeneratePrimaryKeyScript(destinateTable));

            sb.AppendLine(");")
                .AppendLine("END");

            return sb.ToString();
        }

        /// <summary>
        /// build foreign key constraint script
        /// </summary>
        /// <param name="destinateTable"></param>
        /// <returns></returns>
        public string ConstructRelationship(Type destinateTable)
        {
            Func<Type, Type, bool> compareTypesFunc =
                (source, compare) =>
                {
                    var result = false;
                    if (source == compare)
                    {
                        result = true;
                    }
                    else
                    {
                        var currentType = compare.BaseType;
                        while (currentType != null)
                        {
                            if (!currentType.IsGenericType || currentType.GetGenericTypeDefinition() != source)
                            {
                                currentType = currentType.BaseType;
                            }
                            else
                            {
                                result = true;
                                break;
                            }                                
                        }
                        
                    }
                    return result;
                };

            var sb = new StringBuilder();
            var primaryTable = destinateTable.GetCustomAttribute<TableNameAttribute>()?.Value;
            var primaryKey = destinateTable.GetCustomAttribute<PrimaryKeyAttribute>()?.Value;
            var foreignKeys =
                destinateTable.GetProperties().Where(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>)).ToList();
            foreach (var foreignKey in foreignKeys)
            {
                //determine foreign key table (i.e. ICollection<UserLogin> => UserLogin)
                var type = foreignKey.PropertyType.GetGenericArguments().FirstOrDefault();
                if (type == null) continue;

                //find foreign key table
                var foreignKeyTable = type.GetCustomAttribute<TableNameAttribute>();
                //find foreign key property
                var foreignKeyColumn =
                    type.GetProperties()
                        .FirstOrDefault(p => compareTypesFunc(p.GetCustomAttribute<ForeignKeyAttribute>()?.Reference, destinateTable));

                if (foreignKeyColumn == null) continue;
                var constraint = $"FK_dbo.{foreignKeyTable?.Value}_dbo.{primaryTable}_{primaryKey}";

                sb.AppendLine(
                    $"IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = '{constraint}')");
                sb.AppendLine("BEGIN");
                sb.AppendLine(
                    $"ALTER TABLE {foreignKeyTable?.Value} ADD CONSTRAINT [{constraint}] FOREIGN KEY ([{foreignKeyColumn.GetCustomAttribute<ColumnAttribute>()?.Name ?? foreignKeyColumn.Name}]) REFERENCES [dbo].[{primaryTable}] ([{primaryKey}])  ON DELETE CASCADE");
                sb.AppendLine("END");
                sb.AppendLine();
            }
            return sb.ToString();
        }

        /// <summary>
        /// map each property to sql script
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private string GenerateColumnScript(PropertyInfo property)
        {
            var mappings = new Dictionary<Type, string>
            {
                { typeof(int), "INT NOT NULL"},
                { typeof(int?), "INT NULL"},
                {typeof(Guid), "UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL" },
                {typeof(Guid?), "UNIQUEIDENTIFIER NULL" },
                {typeof(string), "NVARCHAR(MAX) NULL" },
                {typeof(bool), "BIT NOT NULL" },
                {typeof(bool?), "BIT NULL" },
                {typeof(DateTime), "DATETIME NOT NULL" },
                {typeof(DateTime?), "DATETIME NULL" }
            };
            if (mappings.All(p => p.Key != property.PropertyType)) return string.Empty;

            return $"[{property.Name}] {mappings.FirstOrDefault(p => p.Key == property.PropertyType).Value},";
        }

        /// <summary>
        /// generate foreign key script
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private string GenerateForeignKeyScript(PropertyInfo property, Type table)
        {
            var foreignKeyInfo = property.GetCustomAttribute<ForeignKeyAttribute>();
            if (foreignKeyInfo?.Reference == null) return string.Empty;

            var tableName = table.GetCustomAttribute<TableNameAttribute>();

            //get primary key table & primary key column
            var primaryKeyTable = foreignKeyInfo.Reference.GetCustomAttribute<TableNameAttribute>()?.Value;
            var referenceKey =
                foreignKeyInfo.Reference.GetCustomAttribute<PrimaryKeyAttribute>()?.Value;
            var onCascadeDelete = foreignKeyInfo.OnCascadeDelete ? "ON DELETE CASCADE" : string.Empty;

            //get current foreign key column
            var foreignKeyColumn = property.GetCustomAttribute<ColumnAttribute>().Name ?? property.Name;

            //could not find primary key annotation on Reference table
            if (string.IsNullOrEmpty(referenceKey)) return string.Empty;


            return
                $"CONSTRAINT [FK_dbo.{tableName}_dbo.{primaryKeyTable}_{foreignKeyColumn}] FOREIGN KEY ([{foreignKeyColumn}]) REFERENCES [dbo].[{primaryKeyTable}] ([{referenceKey}])  {onCascadeDelete},";
        }

        private string GeneratePrimaryKeyScript(Type table)
        {
            var pkInfo = table.GetCustomAttribute<PrimaryKeyAttribute>();

            return $"CONSTRAINT [PK_dbo.{table.Name}] PRIMARY KEY CLUSTERED ([{pkInfo.Value}] ASC)";
        }
    }

    public class IdentityDbContext<TUser> :
            IdentityDbContext<TUser, IdentityRole, Guid, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
        where TUser : IdentityUser
    {
        public IdentityDbContext() : base()
        {

        }

        public IdentityDbContext(string connectionString) : base(connectionString)
        {

        }
    }
}
