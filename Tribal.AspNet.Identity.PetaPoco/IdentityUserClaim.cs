﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribal.AspNet.Identity.PetaPoco.Attributes;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco
{
    //
    // Summary:
    //     EntityType that represents one specific user claim
    //
    // Type parameters:
    //   TKey:
    [TableName("AspNetUserClaims")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class IdentityUserClaim<TKey>
    {
        public IdentityUserClaim() { }

        //
        // Summary:
        //     Claim type
        public virtual string ClaimType { get; set; }
        //
        // Summary:
        //     Claim value
        public virtual string ClaimValue { get; set; }
        //
        // Summary:
        //     Primary key
        public virtual int Id { get; set; }
        //
        // Summary:
        //     User Id for the user who owns this login
        [ForeignKey(OnCascadeDelete = true, Reference = typeof(IdentityUser<,,,>))]
        public virtual TKey UserId { get; set; }
    }

    public class IdentityUserClaim : IdentityUserClaim<Guid>
    {
        public IdentityUserClaim() : base()
        {

        }
    }
}
