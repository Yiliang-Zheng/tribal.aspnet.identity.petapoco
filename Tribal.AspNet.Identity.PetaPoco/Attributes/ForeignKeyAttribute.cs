﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tribal.AspNet.Identity.PetaPoco.Attributes
{
    /// <summary>
    ///     Represents an attribute which can decorate a Poco property to mark the property as a column. It may also optionally
    ///     supply the DB column name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ForeignKeyAttribute : Attribute
    {        
        /// <summary>
        /// The reference table of the forign key
        /// </summary>
        public Type Reference { get; set; }

        /// <summary>
        /// flag to indicate whether turn on 'On Cascade Delete' or not
        /// </summary>
        public bool OnCascadeDelete { get; set; }
    }
}
