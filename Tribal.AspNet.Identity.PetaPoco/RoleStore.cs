﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Tribal.AspNet.Identity.PetaPoco.Helper;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco
{
    public class RoleStore<TRole, TKey, TUserRole> : IQueryableRoleStore<TRole, TKey>,
            IRoleStore<TRole, TKey>, IDisposable
        where TRole : IdentityRole<TKey, TUserRole>, new()
        where TUserRole : IdentityUserRole<TKey>, new()
    {
        private Database _dbContext;

        private string RoleTable => typeof(TRole).ToTableName("AspNetRoles");

        private string UserRoleTable => typeof(TUserRole).ToTableName("AspNetUserRoles");

        public RoleStore(Database dbContext)
        {
            _dbContext = dbContext;
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public Task CreateAsync(TRole role)
        {
            if (role == null) throw new ArgumentException("role is null", nameof(role));
            var id = _dbContext.Insert(RoleTable, "Id", role);
            return Task.FromResult(id);
        }

        public Task UpdateAsync(TRole role)
        {
            if (role == null) throw new ArgumentException("role");
            var rows = _dbContext.Update(RoleTable, "Id", role);
            return Task.FromResult(rows > 0);
        }

        public Task DeleteAsync(TRole role)
        {
            if (role == null) throw new ArgumentException("role");
            var rows = _dbContext.Delete(RoleTable, "Id", role);
            return Task.FromResult(rows > 0);
        }

        public Task<TRole> FindByIdAsync(TKey roleId)
        {
            var role = _dbContext.Single<TRole>(roleId);
            return Task.FromResult(role);
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            var role =
                _dbContext.SingleOrDefault<TRole>(new Sql().Select("*").From(RoleTable).Where("Name = @0", roleName));
            return Task.FromResult(role);
        }

        public IQueryable<TRole> Roles { get; }
    }
    
}
