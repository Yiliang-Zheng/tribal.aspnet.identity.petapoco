﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribal.AspNet.Identity.PetaPoco.Attributes;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco
{
    [TableName("AspNetUserLogins")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class IdentityUserLogin<TKey>
    {
        public IdentityUserLogin() { }

        public virtual int Id { get; set; }

        //
        // Summary:
        //     The login provider for the login (i.e. facebook, google)
        public virtual string LoginProvider { get; set; }
        //
        // Summary:
        //     Key representing the login for the provider
        public virtual string ProviderKey { get; set; }
        //
        // Summary:
        //     User Id for the user who owns this login        
        [ForeignKey(Reference = typeof(IdentityUser<,,,>))]
        public virtual TKey UserId { get; set; }
    }

    public class IdentityUserLogin : IdentityUserLogin<Guid>
    {
        public IdentityUserLogin() : base()
        {

        }
    }
}
