﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Tribal.AspNet.Identity.PetaPoco.Helper;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco
{
    public class UserStore<TUser, TRole, TKey, TUserLogin, TUserRole, TUserClaim> : IUserLoginStore<TUser, TKey>,
                                                                                    IUserClaimStore<TUser, TKey>,
                                                                                    IUserRoleStore<TUser, TKey>,
                                                                                    IUserPasswordStore<TUser, TKey>,
                                                                                    IUserSecurityStampStore<TUser, TKey>,
                                                                                    IQueryableUserStore<TUser, TKey>,
                                                                                    IUserEmailStore<TUser, TKey>,
                                                                                    IUserPhoneNumberStore<TUser, TKey>,
                                                                                    IUserTwoFactorStore<TUser, TKey>,
                                                                                    IUserLockoutStore<TUser, TKey>,
                                                                                    IUserStore<TUser, TKey>
    where TUser : IdentityUser<TKey, TUserLogin, TUserRole, TUserClaim>
    where TRole : IdentityRole<TKey, TUserRole>
    where TUserLogin : IdentityUserLogin<TKey>, new()
    where TUserRole : IdentityUserRole<TKey>, new()
    where TUserClaim : IdentityUserClaim<TKey>, new()
    {
        private Database _dbContext;
        /// <summary>
        /// User table name
        /// </summary>
        private string UserTable => typeof(TUser).ToTableName("AspNetUsers");

        /// <summary>
        /// role table name
        /// </summary>
        private string RoleTable => typeof(TRole).ToTableName("AspNetRoles");

        /// <summary>
        /// user login table name
        /// </summary>
        private string UserLoginTable => typeof(TUserLogin).ToTableName("AspNetUserLogins");

        /// <summary>
        /// user role table name
        /// </summary>
        private string UserRoleTable => typeof(TUserRole).ToTableName("AspNetUserRoles");

        /// <summary>
        /// user cliams table name
        /// </summary>
        private string UserClaimTable => typeof(TUserClaim).ToTableName("AspNetUserClaims");


        public UserStore(Database dbContext)
        {
            _dbContext = dbContext;
        }
        public Task CreateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            var id = (TKey)_dbContext.Insert(UserTable, "Id", true, user);
            return Task.FromResult(id);
        }

        public Task UpdateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var rowAffected = _dbContext.Update(user, user.Id);
            return Task.FromResult(rowAffected);
        }

        public Task DeleteAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            var rowAffected = _dbContext.Delete<TUser>(user.Id);
            return Task.FromResult(rowAffected);
        }

        public Task<TUser> FindByIdAsync(TKey userId)
        {
            var user = _dbContext.SingleOrDefault<TUser>(userId);
            return Task.FromResult(user);
        }

        public Task<TUser> FindByNameAsync(string userName)
        {
            var user =
                _dbContext.SingleOrDefault<TUser>(new Sql().Select("Top 1 *")
                    .From(UserTable)
                    .Where("UserName = @0", userName));
            return Task.FromResult(user);
        }

        public Task AddLoginAsync(TUser user, UserLoginInfo login)
        {
            if (user == null) throw new ArgumentException("user is null");
            var inserted = new TUserLogin
            {
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey,
                UserId = user.Id
            };
            _dbContext.Insert(UserLoginTable, inserted);
            return Task.FromResult<object>(null);
        }

        public Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            if (user == null || login == null) throw new ArgumentException("user or login is null");
            var result =
                _dbContext.Delete<TUserLogin>(new Sql().Where("LoginProvider=@0 AND ProviderKey=@1 AND UserId=@2",
                    login.LoginProvider, login.ProviderKey, user.Id));
            return Task.FromResult(result);
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null");

            var result =
                _dbContext.Fetch<TUserLogin>(new Sql().Select("*")
                    .From(UserLoginTable)
                    .Where(@"UserId = @0", user.Id))
                    .Select(p => new UserLoginInfo(p.LoginProvider, p.ProviderKey)).ToList();
            return Task.FromResult<IList<UserLoginInfo>>(result);
        }

        public Task<TUser> FindAsync(UserLoginInfo login)
        {
            if (login == null) throw new ArgumentException("login is null");

            var user = _dbContext.FirstOrDefault<TUser>(new Sql().Select($"{UserTable}.*")
                .From(UserTable)
                .InnerJoin(UserLoginTable)
                .On($"{UserTable}.UserId = {UserLoginTable}.Id")
                .Where("LoginProvider = @0 AND ProviderKey = @1", login.LoginProvider, login.ProviderKey));
            return Task.FromResult(user);
        }

        public Task<IList<Claim>> GetClaimsAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            var claims =
                _dbContext.Query<TUserClaim>(
                    new Sql().Select($"{UserClaimTable}.*")
                        .From(UserClaimTable)
                        .InnerJoin(UserTable)
                        .On($"{UserClaimTable}.UserId = {UserTable}.Id")
                        .Where("UserId = @0", user.Id))
                        .Select(p => new Claim(p.ClaimType, p.ClaimValue)).ToList();
            return Task.FromResult<IList<Claim>>(claims);
        }

        public Task AddClaimAsync(TUser user, Claim claim)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            if (claim == null) throw new ArgumentException("claim is null", nameof(claim));

            var inserted = new TUserClaim
            {
                ClaimType = claim.Type,
                ClaimValue = claim.Value,
                UserId = user.Id
            };
            var id = _dbContext.Insert("AspNetUserClaims", "Id", inserted);
            return Task.FromResult(id);
        }

        public Task RemoveClaimAsync(TUser user, Claim claim)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            if (claim == null) throw new ArgumentException("claim is null", nameof(claim));

            var tobeDelete = new TUserClaim
            {
                UserId = user.Id,
                ClaimValue = claim.Value,
                ClaimType = claim.Type
            };
            var result = _dbContext.Delete(UserClaimTable, "Id", tobeDelete);
            return Task.FromResult(result);
        }

        public Task AddToRoleAsync(TUser user, string roleName)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            if (string.IsNullOrEmpty(roleName)) throw new ArgumentException("role name is empty", nameof(roleName));

            using (var transaction = _dbContext.GetTransaction())
            {
                var role =
                    _dbContext.FirstOrDefault<TRole>(new Sql().Select("*")
                        .From(RoleTable)
                        .Where("Name = @0", roleName));

                if (role == null) throw new ArgumentException("role is null", nameof(role));
                var result = _dbContext.Insert(UserRoleTable, new { UserId = user.Id, RoleId = role.Id });
                transaction.Complete();
                return Task.FromResult(result);
            }
        }

        public Task RemoveFromRoleAsync(TUser user, string roleName)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            if (string.IsNullOrEmpty(roleName)) throw new ArgumentException("role name is empty", nameof(roleName));

            using (var transaction = _dbContext.GetTransaction())
            {
                var role =
                    _dbContext.FirstOrDefault<TRole>(new Sql().Select("*")
                        .From(RoleTable)
                        .Where("Name = @0", roleName));

                if (role == null) throw new ArgumentException("role is null", nameof(role));
                var result = _dbContext.Insert(UserRoleTable, new { UserId = user.Id, RoleId = role.Id });
                transaction.Complete();
                return Task.FromResult(result);
            }
        }

        public Task<IList<string>> GetRolesAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            var roles =
                _dbContext.Fetch<TRole>(
                    new Sql().Select($"{RoleTable}.*")
                        .From(RoleTable)
                        .InnerJoin(UserRoleTable)
                        .On($"{UserRoleTable}.RoleId = {RoleTable}.Id")
                        .Where($"{UserRoleTable}.UserId = @0", user.Id)).Select(p => p.Name).ToList();
            return Task.FromResult<IList<string>>(roles);
        }

        public Task<bool> IsInRoleAsync(TUser user, string roleName)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            if (string.IsNullOrEmpty(roleName)) throw new ArgumentException("role name is empty", nameof(roleName));

            var result =
                _dbContext.FirstOrDefault<TUserRole>(
                    new Sql().Select("*")
                        .From(UserRoleTable)
                        .InnerJoin(RoleTable)
                        .On($"{UserRoleTable}.RoleId = {RoleTable}.Id")
                        .Where($"{UserRoleTable}.UserId = @0 AND {RoleTable}.Name = @1", user.Id, roleName));
            return Task.FromResult(result != null);
        }

        public Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            if (string.IsNullOrEmpty(passwordHash)) throw new ArgumentException("password is empty", nameof(passwordHash));

            //using (var transaction = _dbContext.GetTransaction())
            //{
            //    var item =
            //        _dbContext.SingleOrDefault<TUser>(new Sql().Select("*")
            //            .From(UserTable)
            //            .Where("Id = @0", user.Id));

            //    transaction.Complete();
            //}
            user.PasswordHash = passwordHash;
            //_dbContext.Save(user);
            return Task.FromResult<object>(null);
        }

        public Task<string> GetPasswordHashAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            var item =
                _dbContext.SingleOrDefault<TUser>(new Sql().Select("*").From(UserTable).Where("Id = @0", user.Id));
            return Task.FromResult(item?.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            var item =
                _dbContext.SingleOrDefault<TUser>(new Sql().Select("*").From(UserTable).Where("Id = @0", user.Id));
            return Task.FromResult(!string.IsNullOrEmpty(item?.PasswordHash));
        }

        public Task SetSecurityStampAsync(TUser user, string stamp)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            //var item =
            //    _dbContext.SingleOrDefault<TUser>(new Sql().Select("*").From(UserTable).Where("Id = @0", user.Id));

            user.SecurityStamp = stamp;

            //_dbContext.Save(user);
            return Task.FromResult(user);
        }

        public Task<string> GetSecurityStampAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            var stamp =
                _dbContext.SingleOrDefault<string>(new Sql().Select("SecurityStamp")
                    .From(UserTable)
                    .Where("Id = @0", user.Id));

            return Task.FromResult(stamp);
        }

        public IQueryable<TUser> Users { get; }
        public Task SetEmailAsync(TUser user, string email)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            user.Email = email;
            return Task.FromResult<object>(null);
        }

        public Task<string> GetEmailAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            //var item =
            //    _dbContext.SingleOrDefault<string>(new Sql().Select("Email")
            //        .From(UserTable)
            //        .Where("Id = @0", user.Id));
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            //var item =
            //    _dbContext.SingleOrDefault<bool>(new Sql().Select("EmailConfirmed")
            //        .From(UserTable)
            //        .Where("Id = @0", user.Id));
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(TUser user, bool confirmed)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            user.EmailConfirmed = confirmed;
            //var rows = _dbContext.Update(user, new List<string> { "EmailConfirmed" });
            return Task.FromResult<object>(null);
        }

        public Task<TUser> FindByEmailAsync(string email)
        {
            var user =
                _dbContext.SingleOrDefault<TUser>(new Sql().Select("*").From(UserTable).Where("Email = @0", email));
            return Task.FromResult(user);
        }

        public Task SetPhoneNumberAsync(TUser user, string phoneNumber)
        {
            user.PhoneNumber = phoneNumber;
            //var rows = _dbContext.Update(UserTable, "Id", user, user.Id, new List<string> { "PhoneNumber" });
            return Task.FromResult(user);
        }

        public Task<string> GetPhoneNumberAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            return Task.FromResult(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            return Task.FromResult(user.PhoneNumberConfirmed);
        }

        public Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            user.PhoneNumberConfirmed = confirmed;
            //var rows = _dbContext.Update(UserTable, "Id", user, user.Id, new List<string> { "PhoneNumberConfirmed" });
            return Task.FromResult(user);
        }

        public Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            user.TwoFactorEnabled = enabled;
            //var rows = _dbContext.Update(UserTable, "Id", user, user.Id, new List<string> { "TwoFactorEnabled" });
            return Task.FromResult(user);
        }

        public Task<bool> GetTwoFactorEnabledAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            return Task.FromResult(user.TwoFactorEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            return Task.FromResult(user.LockoutEndDateUtc ?? default(DateTimeOffset));
        }

        public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            user.LockoutEndDateUtc = lockoutEnd.DateTime;
            //var rows = _dbContext.Update(UserTable, "Id", user, user.Id, new List<string> { "LockoutEndDateUtc" });
            return Task.FromResult(user);
        }

        public Task<int> IncrementAccessFailedCountAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            user.AccessFailedCount++;
            //var rows = _dbContext.Update(UserTable, "Id", user, user.Id, new List<string> { "AccessFailedCount" });
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task ResetAccessFailedCountAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            user.AccessFailedCount = 0;
            //var rows = _dbContext.Update(UserTable, "Id", user, user.Id, new List<string> { "AccessFailedCount" });
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<int> GetAccessFailedCountAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> GetLockoutEnabledAsync(TUser user)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            return Task.FromResult(user.LockoutEnabled);
        }

        public Task SetLockoutEnabledAsync(TUser user, bool enabled)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));
            user.LockoutEnabled = enabled;
            //var rows = _dbContext.Update(UserTable, "Id", user, user.Id, new List<string> { "LockoutEnabled" });
            //_dbContext.Save(user);
            return Task.FromResult<object>(null);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }

    public class UserStore<TUser> : UserStore<TUser, IdentityRole, Guid, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>, IUserStore<TUser, Guid>, IDisposable where TUser : IdentityUser
    {
        //
        // Summary:
        //     Constructor
        //
        // Parameters:
        //   context:
        public UserStore(Database context) : base(context) { }
    }
}
