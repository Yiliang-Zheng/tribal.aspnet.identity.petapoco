﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco
{
    /// <summary>
    ///     Represents a Role entity
    /// </summary>
    /// <typeparam name="TKey">TKey</typeparam>
    /// <typeparam name="TUserRole">TUserRole</typeparam>
    [TableName("AspNetRoles")]
    [PrimaryKey("Id")]
    public class IdentityRole<TKey, TUserRole> : IRole<TKey>
        where TUserRole : IdentityUserRole<TKey>
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        public IdentityRole()
        {
        }

        /// <summary>
        ///     Constructor with name param
        /// </summary>
        public IdentityRole(string name) : this()
        {
            Name = name;
        }

        /// <summary>
        ///     Constructor with name and id params
        /// </summary>
        public IdentityRole(string name, TKey id)
        {
            Name = name;
            Id = id;
        }

        /// <summary>
        ///     Role id
        /// </summary>
        public TKey Id { get; set; }

        /// <summary>
        ///     Role Name
        /// </summary>
        public string Name { get; set; }

        //
        // Summary:
        //     Navigation property for users in the role
        [Ignore]
        public virtual ICollection<TUserRole> Users { get; }
    }

    public class IdentityRole : IdentityRole<Guid, IdentityUserRole>
    {
        public IdentityRole():base()
        {
            
        }
    }
}
