﻿using System;
using System.Reflection;
using System.Security.Principal;
using Microsoft.AspNet.Identity;
using Tribal.Data.Entity;

namespace Tribal.AspNet.Identity.PetaPoco.Helper
{
    public static class ExtenstionClass
    {
        public static string ToTableName(this Type pocoClass, string defaultName = null)
        {
            var tableName = pocoClass.GetCustomAttribute<TableNameAttribute>()?.Value ?? defaultName;
            return tableName;
        }

        public static Guid ToGuid(this string input)
        {
            return Guid.Parse(input);
        }

        //
        // Summary:
        //     Return the user id using the UserIdClaimType
        //
        // Parameters:
        //   identity:
        public static Guid GetUserIdAsGuid(this IIdentity identity)
        {
            var userId = identity.GetUserId();
            return userId.ToGuid();
        }
    }
}
