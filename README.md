# README #
ASP.NET Identity PetaPoco is implemented ASP.NET Identity by using PetaPoco.
PetaPoco is a tiny, fast, single-file micro-ORM for .NET and Mono. 
If user wants to explore more about PetaPoco, please [Click Here](https://github.com/CollaboratingPlatypus/PetaPoco)

ASP.NET Identity is the new membership system for building ASP.NET web applications. ASP.NET Identity allows you to add login features to your application and makes it easy to customize data about the logged in user.